# Inzynierski projekt zespolowy
## Mazowiecka Uczelnia Publiczna w Płocku
## 4 rok

# Członkowie grupy
Paulina Kobieracka

Aleksandra Kuczmarska

Dominik Oszal

Gabriel Jędrzejczak

Hubert Machała

Mateusz Zgondek

# Endpointy API

`[POST] http://machala.net.pl/mup-project/api/public/auth (przyjmuje login i haslo) (zwraca obiekt z tokenem lub obiekt z błędem`

`[GET] http://machala.net.pl/mup-project/api/public/eksponaty (nie przyjmuje wartosci) (zwraca tablice)`

`[GET] http://machala.net.pl/mup-project/api/public/eksponaty/{id} (przyjmuje id eksponatu) (zwraca obiekt)`

`[POST] http://machala.net.pl/mup-project/api/public/eksponaty (przyjmuje tytul, opis, url_grafika) (dodaje eksponat do bazy danych i zwraca informacje o powodzeniu lub bledzie w formacie JSON)`

`[PUT] http://machala.net.pl/mup-project/api/public/eksponaty/{id} (przyjmuje id eksponatu, tytul, opis, url_grafika) (aktualizuje eksponat w bazie danych i zwraca informacje o powodzeniu lub bledzie w formacie JSON)`

`[DELETE] http://machala.net.pl/mup-project/api/public/eksponaty/{id} (przyjmuje id eksponatu) (kasuje eksponat z bazy danych i zwraca informacje o powodzeniu lub bledzie w formacie JSON)`
