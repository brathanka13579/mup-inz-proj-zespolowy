<?php
$json = file_get_contents($API.'/eksponaty');
$eksponaty = json_decode($json);
?>

<div class="container">
<div class="row justify-content-center">
  <div class="col-lg-8">
<div class="jumbotron jumbotron-fluid" style="margin: 50px 0;">
  <div class="container">
    <h1 class="display-4">Witamy na stronie Muzeum</h1>
    <p class="lead">Poniej znajdują się eksponaty</p>
  </div>
</div>
</div>
</div>


<div class="container">
<div class="row justify-content-center">
  <div class="col-lg-8">
<?php
for($i = 0; $i < count($eksponaty); $i++) {
    ?>
    <div class="card">
  <img class="card-img-top" src="<?php echo $eksponaty[$i]->url_grafika; ?>" alt="<?php echo $eksponaty[$i]->tytul; ?>">
  <div class="card-body">
    <h5 class="card-title"><?php echo $eksponaty[$i]->tytul; ?></h5>
    <p class="card-text"><?php echo $eksponaty[$i]->opis; ?></p>
    <a href="index.php?eksponat=<?php echo $eksponaty[$i]->id; ?>" class="btn btn-primary">Przejdź do eksponatu</a>
  </div>
  
<?php } ?>

</div>
</div>
</div>