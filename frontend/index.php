<?php
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
include_once("config.php");
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inzynierski Projekt</title>
    <meta name="description" content="Grupowy projekt inynierski - 4 rok - Mazowiecka Uczelnia Publiczna w Płocku" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    
<?php
if($_GET['eksponat']) {
  include("inc/eksponat.php");
} else {
  include("inc/home.php");
}
?>

<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>