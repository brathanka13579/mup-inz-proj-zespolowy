<?php
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
session_start();
include_once("../config.php");
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin - Inzynierski Projekt</title>
    <meta name="description" content="Admin - Grupowy projekt inynierski - 4 rok - Mazowiecka Uczelnia Publiczna w Płocku" />
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    
<?php
if($_SESSION['token-mup-projekt']) {
?>

  <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Panel Admina - Grupowy Projekt Inzynierski</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="<?php echo $site; ?>/admin/index.php?site=logout">Wyloguj</a>
        </li>
      </ul>
    </nav>


    <div class="container-fluid">
      <div class="row">
        <nav class="col-4 col-md-3 col-lg-2 d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  Strona główna
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  Członkowie grupy
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  Dokumentacja API / linki
                </a>
              </li>
            </ul>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Eksponaty</span>
              <a class="d-flex align-items-center text-muted" href="#">
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  Lista eksponatów
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  Dodaj eksponat
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="ml-sm-auto px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">


<?php
if($_GET['site']) {
  include('inc/loggedin/'.$_GET['site'].'.php');
} else {
  include('inc/loggedin/home.php');
}
?>

</div>
        </main>
</div>
</div>

<?php
} else {
  include('inc/loggedout/login.php');
}
?>

<script src="../js/bootstrap.bundle.min.js"></script>
</body>
</html>