<div class="wrapper fadeInDown">
  <div id="formContent">

    <div class="fadeIn first">
      <h1>Panel Administracyjny</h1>
      <h2>Grupowy projekt inzynierski</h2>
      <h2>Mazowiecka Uczelnia Publicza w Płocku</h2>
      <h2>4 Rok</h2>
      <?php
$url = $API.'/auth';


if(isset($_POST['loguj'])) {
  $login = trim($_POST['login']);
  $haslo = $_POST['haslo'];
  $data = array('login' => $login, 'haslo' => $haslo);

  $context = stream_context_create(array(
    'http' => array(
      'ignore_errors' => true,
      'method' => "POST",
      'header'=>  "Content-Type: application/json\r\n" .
      "Accept: application/json\r\n",
      'content' => json_encode($data)
    )
));

$content = file_get_contents($url, false, $context);
$result = json_decode($content, true);

if(isset($result["error"])) {
  wypisz_blad($result["error"]);
} else {
  wypisz_sukces("Logowanie przebieglo pomyslnie");
  $token = $result["token"];
  $_SESSION['token-mup-projekt'] = $token;
  header( 'refresh:3;url='.$site.'/admin/index.php' );
}



}

?>
    </div> 

    <form method="POST" action="">
      <input type="text" id="login" class="fadeIn second" name="login" placeholder="login">
      <input type="text" id="password" class="fadeIn third" name="haslo" placeholder="hasło">
      <input type="submit" class="fadeIn fourth" name="loguj" value="Zaloguj">
    </form>

  </div>
</div>