<?php

namespace App\Http\Controllers;

use App\Models\Uzytkownik;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * The request instance.
     * @var Request
     */
    private $request;

    /**
     * Create a new controller instance.
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Create a new token.
     * @param  Uzytkownik $user
     * @return string
     */
    protected function jwt(Uzytkownik $user)
    {
        $payload = [
            'iss' => "mup-projekt", // Issuer of the token
            'sub_id' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            // 'exp' => time() + 60 * 60 // Expiration time TODO na produkcji
            'exp' => time() + 60 * 60 * 10000
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * @param Uzytkownik $user
     * @return mixed
     * @throws ValidationException
     */
    public function verify(Uzytkownik $user)
    {
        // Find the user by email
        $user = Uzytkownik::where('login', $this->request->input('login'))->first();
        if (!$user) {
            return response()->json([
                'error' => 'Login does not exist.'
            ], 400);
        }
        // Verify the password and generate the token
        if (md5($this->request->input('haslo')) == $user->haslo) {
            return response()->json([
                'token' => $this->jwt($user)
            ], 200);
        }
        // Bad Request response
        return response()->json([
            'error' => 'Login or password is wrong.',
        ], 400);
    }

    public function verify_get() {
        return response()->json([
            'error' => 'Metoda GET niedozwolona. Musisz przekazac dane przez POST'
        ], 400);
    }
}